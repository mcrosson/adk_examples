Components
==========
  Arduino Mega ADK
  Parallax HMC5883L 3-axis compass
  Parallax L3G4200D gyroscope
  SparkFun bluetooth mate silver (WRL-10393)

Software
========
  Arduino IDE
  Android ADK
  Amarino Android library (http://www.amarino-toolkit.net/)

Pre-Flight Setup
================
  Download the ADK using repo
  Copy adk1/board/AndroidAccessory and USB_Host_Shield to your ~/Arduino/libraries or ~/sketchbook/libraries folder
  Install Google APIs for the platform you plan on building for
  Build sources and apk