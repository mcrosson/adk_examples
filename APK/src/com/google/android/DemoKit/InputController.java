package com.google.android.DemoKit;

import java.text.DecimalFormat;

import android.widget.TextView;

public class InputController extends AccessoryController {
	private TextView mCompass;
	private TextView mCompassCount;
	
	private TextView mGyro;
	private TextView mGyroCount;
	
	int compassCount = 0;
	int gyroCount = 0;
	
	private final DecimalFormat mCompassFormatter = new DecimalFormat("###.00" + (char)0x00B0);

	InputController(DemoKitActivity hostActivity) {
		super(hostActivity);
		mCompass = (TextView) findViewById(R.id.compassValue);
		mCompassCount = (TextView) findViewById(R.id.compassValueCount);
		mGyro = (TextView) findViewById(R.id.gyroValue);
		mGyroCount = (TextView) findViewById(R.id.gyroValueCount);
	}

	protected void onAccesssoryAttached() {
	}

	public void setCompass(float compassFromArduino) {
		compassCount++;
		mCompass.setText(mCompassFormatter.format(compassFromArduino));
		mCompassCount.setText(Integer.toString(compassCount));
	}
	
	public void setGyro(String value) {
		gyroCount++;
		mGyro.setText(value);
		mGyroCount.setText(Integer.toString(gyroCount));
	}
}
