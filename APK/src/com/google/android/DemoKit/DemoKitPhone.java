package com.google.android.DemoKit;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

public class DemoKitPhone extends BaseActivity implements OnClickListener {
	static final String TAG = "DemoKitPhone";
	/** Called when the activity is first created. */
	LinearLayout mInputContainer;

	@Override
	protected void hideControls() {
		super.hideControls();
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	protected void showControls() {
		super.showControls();
		mInputContainer = (LinearLayout) findViewById(R.id.inputContainer);
		showTabContents(true);
	}

	void showTabContents(Boolean showInput) {
		if (showInput) {
			mInputContainer.setVisibility(View.VISIBLE);
		} else {
			mInputContainer.setVisibility(View.GONE);
		}
	}

	public void onClick(View v) {
	}

}