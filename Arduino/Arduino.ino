// Reference the I2C Library
#include <Wire.h>

// Reference the HMC5883L Compass Library
#include "HMC5883L.h"
// Reference the L3G4200D library
#include "L3G.h"

// Android ADK accessory includes
#include <Max3421e.h>
#include <Usb.h>
#include <AndroidAccessory.h>

// Accessory init code
AndroidAccessory acc("Google, Inc.",
         "DemoKit",
         "DemoKit Arduino Board",
         "1.0",
         "http://www.android.com",
         "0000000012345678");

// Store our compass as a variable.
HMC5883L compass;
// Record any errors that may occur in the compass.
int compassError = 0;

// Store our gyroscope as a variable
L3G gyro;

// Communication with Android via BT library
#include "MeetAndroid.h"
MeetAndroid meetAndroid;

// Temp variables used for output / Amarino send
char* gyroXString = new char[8];
char* gyroYString = new char[8];
char* gyroZString = new char[8];
char* compassHeadingString = new char[8];

// Device init functions (defined below)
void initGyro();
void initCompass();

// Output functions (defined below)
//   Print data to serial, send to Android via ADK and Amarino
void outputGyro();
void outputCompass();

// Out setup routine, here we will configure the microcontroller, compass and LCD
void setup()
{
  // Initialize serial port -> PC
  Serial.begin(9600);
  
  // Initialize the hardware pins serial port for BT communication
  Serial3.begin(115200);

  // Start the I2C interface.
  Wire.begin(); 

  // Initialize sensors
  initGyro();
  initCompass();

  // Setup the ADK part of the build/device
  acc.powerOn();
}

// Our main program loop.
void loop()
{
  // Print the gyro values
  outputGyro();

  // Print the compass values
  outputCompass();

  // Read from amarino to ensure communication is right
  meetAndroid.receive();

  // Delay so we only send so fast
  delay(1000);
}

void outputGyro() {
  gyro.read();

  // // Print the values via serial connection
  Serial.print("Gyro: ");
  Serial.print("X:");
  ftoa(gyroXString, gyro.g.x, 2);
  Serial.print(gyroXString);
  Serial.print(" Y:");
  ftoa(gyroYString, gyro.g.y, 2);
  Serial.print(gyroYString);
  Serial.print(" Z:");
  ftoa(gyroZString, gyro.g.z, 2);
  Serial.println(gyroZString);

  // // Send the degrees reading to Android via BT
  String toSend = "Gyro: ";
  toSend += "X:";
  toSend += gyroXString;
  toSend += " Y:";
  toSend += gyroYString;
  toSend += " Z:";
  toSend += gyroZString;
  // Get C string for send operation
  int totalSize = 40;
  char* amarinoString = new char[totalSize];
  toSend.toCharArray(amarinoString, totalSize);
  meetAndroid.send(amarinoString);

  // Send the gyro x,y,z values to android
  byte msg[13];
  byte float_array[4];
  
  if (acc.isConnected()) {
    msg[0] = 0x5;

    // X Value
    memcpy(float_array, &gyro.g.x, sizeof(gyro.g.x));
    msg[1] = float_array[0];
    msg[2] = float_array[1];
    msg[3] = float_array[2];
    msg[4] = float_array[3];

    // Y Value
    memcpy(float_array, &gyro.g.y, sizeof(gyro.g.y));
    msg[5] = float_array[0];
    msg[6] = float_array[1];
    msg[7] = float_array[2];
    msg[8] = float_array[3];

    // Z Value
    memcpy(float_array, &gyro.g.z, sizeof(gyro.g.z));
    msg[9] = float_array[0];
    msg[10] = float_array[1];
    msg[11] = float_array[2];
    msg[12] = float_array[3];

    acc.write(msg, 13);
  }
}

// Output the compass data
void outputCompass() {
  // Retrive the raw values from the compass (not scaled).
  MagnetometerRaw raw = compass.ReadRawAxis();
  // Retrived the scaled values from the compass (scaled to the configured scale).
  MagnetometerScaled scaled = compass.ReadScaledAxis();

  // Calculate heading when the magnetometer is level, then correct for signs of axis.
  float heading = atan2(scaled.YAxis, scaled.XAxis);

  // Once you have your heading, you must then add your 'Declination Angle', which is the 'Error' of the magnetic field in your location.
  // Find yours here: http://www.magnetic-declination.com/
  // Mine is: 2� 37' W, which is 2.617 Degrees, or (which we need) 0.0456752665 radians, I will use 0.0457
  // If you cannot find your Declination, comment out these two lines, your compass will be slightly off.
  //float declinationAngle = 0.0457;
  //heading += declinationAngle;

  // Correct for when signs are reversed.
  if(heading < 0)
    heading += 2*PI;

  // Check for wrap due to addition of declination.
  if(heading > 2*PI)
    heading -= 2*PI;

  // Convert radians to degrees for readability.
  float headingDegrees = heading * 180/M_PI; 

  // Print the values via serial connection
  Serial.print("Compass: ");
  Serial.print(headingDegrees);
  Serial.println(" Degrees   \t");

  // Send the degrees reading to Android via BT
  ftoa(compassHeadingString, headingDegrees, 2);
  String toSend = "Compass: ";
  toSend += compassHeadingString;
  // Get C string for send operation
  int totalSize = 17;
  char* amarinoString = new char[totalSize];
  toSend.toCharArray(amarinoString, totalSize);
  meetAndroid.send(amarinoString);

  // Send the degrees reading (float) to Android via ADK
  byte msg[5];
  byte float_array[4];
  
  if (acc.isConnected()) {
    // break float into 4 bytes for transmission
    memcpy(float_array, &headingDegrees, sizeof(headingDegrees));

    msg[0] = 0x4;
    msg[1] = float_array[0];
    msg[2] = float_array[1];
    msg[3] = float_array[2];
    msg[4] = float_array[3];
    acc.write(msg, 5);
  }
}

// Initialize the gyro
void initGyro() {
  Serial.println("Gyro Init");

  // Create new gyro and initialize it
  gyro = L3G();
  gyro.init();

  // Turns on the L3G's gyro and places it in normal mode.
  gyro.enableDefault();
}

// Initialize the compass
void initCompass() {
  Serial.println("Compass init");
  // Construct a new HMC5883 compass.
  compass = HMC5883L(); 

  // Set the scale of the compass.
  int error = compass.SetScale(1.3); 
  if(error != 0) // If there is an error, print it out.
    Serial.println(compass.GetErrorText(error));

  // Set the measurement mode to Continuous
  error = compass.SetMeasurementMode(Measurement_Continuous);
  if(error != 0) // If there is an error, print it out.
    Serial.println(compass.GetErrorText(error));
}

// Method to convert a flot to character array
char *ftoa(char *a, double f, int precision)
{
  long p[] = {0,10,100,1000,10000,100000,1000000,10000000,100000000};
  
  char *ret = a;
  long heiltal = (long)f;
  itoa(heiltal, a, 10);
  while (*a != '\0') a++;
  *a++ = '.';
  long desimal = abs((long)((f - heiltal) * p[precision]));
  itoa(desimal, a, 10);
  return ret;
}
